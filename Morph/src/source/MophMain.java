package source;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;

import de.Nottrex.Bildverarbeitung.*;

public class MophMain extends JFrame implements Runnable {
	private static final long serialVersionUID = 1L;
	static int Mapweite = 100;
	static int Maphoehe=100;
	static int Malgroesse = 5;
	static int Wahrscheinlichkeit=5;
	static int AbstandX = 10;
	static int AbstandY = 10;
	static int[][] Map;
	
	static JButton Ero = new JButton("Erosion");
	static JButton Dila = new JButton("Dilatation");
	static JButton Kanten = new JButton("Kanten finden");
	static JButton SaveAsPNG = new JButton("Save as .png");
	static JButton SaveAsTXT = new JButton("Save as .txt");
	static JButton ResetMap = new JButton("Reset Map");
	static JButton fillGaps = new JButton("Fill Gaps");
	
	static Insets i;
	
	static BufferedImage Image; 
	
	public MophMain(){
		super("Morphologische Levelgenerierung by PhoenixofForce");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		add(Ero);
		add(Dila);
		add(Kanten);
		add(SaveAsPNG);
		add(SaveAsTXT);
		add(ResetMap);
		add(fillGaps);
		
		File f = new File("Options.txt");
		try {
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			Mapweite = Integer.parseInt(br.readLine());
			Maphoehe = Integer.parseInt(br.readLine());
			Malgroesse = Integer.parseInt(br.readLine());
			Wahrscheinlichkeit = Integer.parseInt(br.readLine());
			AbstandX = Integer.parseInt(br.readLine());
			AbstandY = Integer.parseInt(br.readLine());
			br.close();
			
		} catch (NumberFormatException | IOException e1) {
			e1.printStackTrace();
		}
		
		setLayout(null);
		setVisible(true);
		
		i = getInsets();
		setSize(Mapweite * Malgroesse + 150 + i.left + i.right, Maphoehe * Malgroesse + i.top + i.bottom);
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		final MophMain mm = new MophMain();
		Image = new BufferedImage(Mapweite * Malgroesse + 8, Maphoehe * Malgroesse + 31, BufferedImage.TYPE_INT_RGB);
		Map = new int[Mapweite][Maphoehe];
		
		for(int x = 0; x < Map.length; x++){
			for(int y = 0; y < Map[0].length; y++){
				Random r = new Random();
				int Wahr = r.nextInt(Wahrscheinlichkeit);
				if(Wahr == 0)Map[x][y] = 0;
				else Map[x][y] = 1;
				
				if(x < AbstandX || x > Map.length - AbstandX)Map[x][y] = 1;
				if(y < AbstandY || y > Map[0].length - AbstandY)Map[x][y] = 1;
			}
		}
		
		Ero.setBounds(Mapweite * Malgroesse, 0, 150, Maphoehe * Malgroesse / 7);
		Dila.setBounds(Mapweite * Malgroesse, Maphoehe * Malgroesse / 7 * 1, 150, Maphoehe * Malgroesse / 7);
		Kanten.setBounds(Mapweite * Malgroesse, Maphoehe * Malgroesse / 7 * 2, 150, Maphoehe * Malgroesse / 7);
		SaveAsPNG.setBounds(Mapweite * Malgroesse, Maphoehe * Malgroesse / 7 * 3, 150, Maphoehe * Malgroesse / 7);
		SaveAsTXT.setBounds(Mapweite * Malgroesse, Maphoehe * Malgroesse / 7 * 4, 150, Maphoehe * Malgroesse / 7);
		ResetMap.setBounds(Mapweite * Malgroesse, Maphoehe * Malgroesse / 7 * 5, 150, Maphoehe * Malgroesse / 7);
		fillGaps.setBounds(Mapweite * Malgroesse, Maphoehe * Malgroesse / 7 * 6, 150, Maphoehe * Malgroesse / 7);
		
		Ero.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean[][]Opfer = new boolean[Map.length][Map[0].length];
				for(int x = 0; x < Map.length; x++){
					for(int y = 0; y < Map[0].length; y++){
						if(Map[x][y] == 1)Opfer[x][y] = false;
						else Opfer[x][y] = true;
					}
				}
				
				Opfer=Morphologisch.ErosionBooleanArray(Opfer);
				
				for(int x = 0; x < Map.length; x++){
					for(int y = 0; y < Map[0].length; y++){
						if(Opfer[x][y] == false)Map[x][y] = 1;
						else Map[x][y]=0;
					}
				}
			}			
		});
		
		Dila.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean[][]Opfer = new boolean[Map.length][Map[0].length];
				for(int x = 0; x < Map.length; x++){
					for(int y = 0; y < Map[0].length; y++){
						if(Map[x][y] == 1)Opfer[x][y] = false;
						else Opfer[x][y] = true;
					}
				}
				
				Opfer=Morphologisch.DilatationBooleanArray(Opfer);
				
				for(int x = 0; x < Map.length; x++){
					for(int y = 0; y < Map[0].length; y++){
						if(Opfer[x][y] == false)Map[x][y] = 1;
						else Map[x][y] = 0;
					}
				}
			}			
		});
		
		Kanten.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean[][]Opfer = new boolean[Map.length][Map[0].length];
				for(int x = 0; x < Map.length; x++){
					for(int y = 0; y < Map[0].length; y++){
						if(Map[x][y] == 1)Opfer[x][y] = false;
						else Opfer[x][y] = true;
					}
				}
				
				Opfer=Morphologisch.KantenfindenBooleanArray(Opfer);
				
				for(int x = 0; x < Map.length; x++){
					for(int y = 0; y < Map[0].length; y++){
						if(Opfer[x][y] == false)Map[x][y] = 1;
						else Map[x][y] = 0;
					}
				}
			}			
		});
		
		SaveAsPNG.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				BufferedImage sub = Image.getSubimage(8, 31, Mapweite * Malgroesse, Maphoehe * Malgroesse);
				
				String date = new SimpleDateFormat("MM.dd.yyyy").format(new Date());
				String time = new SimpleDateFormat("HH_mm_ss").format(new Date());
				File f = new File(date +" " + time +".png");
				
				try {
					ImageIO.write(sub, "PNG", f);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}			
		});
		
		SaveAsTXT.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				File f;
				FileWriter fw;	
				String date = new SimpleDateFormat("MM.dd.yyyy").format(new Date());
				String time = new SimpleDateFormat("HH_mm_ss").format(new Date());
				f = new File(date +" " + time +".txt");
				
				try {
					fw = new FileWriter(f, true);
					
					for(int x=0; x<Map.length;x++){
						for(int y=0; y<Map[0].length;y++){
							fw.write(Map[y][x] + " " );
						}
						fw.write(System.getProperty("line.separator"));
					}
					
					fw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}			
		});
		
		ResetMap.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				for(int x = 0; x < Map.length; x++){
					for(int y = 0; y < Map[0].length; y++){
						Random r = new Random();
						int Wahr = r.nextInt(Wahrscheinlichkeit);
						if(Wahr == 0)Map[x][y] = 0;
						else Map[x][y] = 1;
						
						if(x < AbstandX || x > Map.length - AbstandX)Map[x][y] = 1;
						if(y < AbstandY || y > Map[0].length - AbstandY)Map[x][y] = 1;
					}
				}
			}			
		});
		
		fillGaps.addActionListener(new ActionListener(){
			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ArrayList<int[]> Open = new ArrayList<int[]>();
				ArrayList<int[]> Closed = new ArrayList<int[]>();
				ArrayList<ArrayList<int[]>> Rooms = new ArrayList<ArrayList<int[]>>();
				
				int whiteFields = 0;
				int x = 0;
				int y = 0;
				
				for(int x3 = 0; x3 < Map.length; x3++){
					for(int y3 = 0; y3 < Map[0].length; y3++){
						if(Map[x3][y3] == 1){
							whiteFields++;
						}
					}
				}
				
				while(whiteFields > 0){
					for(int x3 = 0; x3 < Map.length; x3++){
						for(int y3 = 0; y3 < Map[0].length; y3++){
							if(Map[x3][y3] == 1){
								x = x3;
								y = y3;
								x3 = Map.length;
								y3 = Map[0].length;
							}
						}
					}
	
					Open.add(new int[]{x,y});
					while(!Open.isEmpty()){
						x = Open.get(0)[0];
						y = Open.get(0)[1];
						Closed.add(Open.get(0));
						if(x >= 1 && !inList(Open, x-1, y) && !inList(Closed, x-1, y) && Map[x-1][y] == 1)Open.add(new int[]{x-1,y});
						if(y >= 1 && !inList(Open, x, y-1) && !inList(Closed, x, y-1) && Map[x][y-1] == 1)Open.add(new int[]{x,y-1});
						if(x < Map.length-1 && !inList(Open, x+1, y) && !inList(Closed, x+1, y) && Map[x+1][y] == 1)Open.add(new int[]{x+1,y});
						if(y < Map[0].length-1 && !inList(Open, x, y+1) && !inList(Closed, x, y+1) && Map[x][y+1] == 1)Open.add(new int[]{x,y+1});
						Closed.add(Open.get(0));
						Open.remove(0);
					}
					
					Rooms.add((ArrayList<int[]>) Closed.clone());
					for(int a = 0; a < Closed.size(); a++){
						Map[Closed.get(a)[0]][Closed.get(a)[1]] = 2;
					}
					Closed.clear();
					
					whiteFields = 0;
					for(int x3 = 0; x3 < Map.length; x3++){
						for(int y3 = 0; y3 < Map[0].length; y3++){
							if(Map[x3][y3] == 1){
								whiteFields++;
							}
						}
					}
				}
				
				int biggestRoom = 0;
				for(int a = 0; a < Rooms.size(); a++){
					if(a == 0)biggestRoom = a;
					if(Rooms.get(biggestRoom).size() < Rooms.get(a).size())biggestRoom = a;
				}
					
				ArrayList<int[]>Room = null;
				if(Rooms.size() > 1){
					for(int a = 0; a < Rooms.size(); a++){
						if(a == biggestRoom)a += 1;
						Room = Rooms.get(a);
						for(int s = 0; s < Room.size(); s++){
							Map[Room.get(s)[0]][Room.get(s)[1]] = 0;
						}
					}		
				}
				
				for(int x1 = 0; x1 < Map.length; x1++){
					for(int y1 = 0; y1 < Map[0].length; y1++){
						if(Map[x1][y1] == 2)Map[x1][y1] = 1;
					}
				}
				
			}			
		});
		
		new Thread(new Runnable(){
			@Override
			public void run() {
				while(true){
					Graphics g = Image.getGraphics();
					Graphics g2 = mm.getGraphics();
					
					for(int x = 0; x < Map.length; x++){
						for(int y = 0; y < Map[0].length; y++){
							if(Map[x][y] == 0){
								g.setColor(Color.BLACK);
							} 	
							else if(Map[x][y] == 1){
								g.setColor(Color.WHITE);
							} 	
							else if(Map[x][y] == 2){
								g.setColor(Color.WHITE);
							} 	
							g.fillRect(x * Malgroesse + i.left, y * Malgroesse + i.top, Malgroesse, Malgroesse);
						}
					}
					
					g2.drawImage(Image, 0, 0, null);
				}
			}
		}).start();
		
	}
	
	public static boolean inList(ArrayList<int[]>Points, int x, int y){
		for(int x2 =0; x2< Points.size(); x2++){
			if(x==Points.get(x2)[0]&&y==Points.get(x2)[1])return true;
		}
		return false;
	}
	
	@Override
	public void run() {
		
	}
}
